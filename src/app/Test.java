package app;

import objects.Adresse;
import objects.Bureau;
import objects.Chercheur;
import objects.Laboratoire;



/**
 * Test
 */
public class Test {

   public static void main(String[] args) {
      
      //2
      Adresse adr;
       
      //3
      adr = new Adresse(20190, "Ain chock", "Casa");

      //4
      adr.afficher();

      //5
      adr.modifier(20200);

      //6
      adr.afficher();

      //7
      Adresse adr2 = new Adresse(20190, "Ain chock", "Casa");

      //8
      adr2.ville = "New york";

      //9
      adr2.afficher();

      //11
      Chercheur chercheur1 = new Chercheur();

      //12
      chercheur1.nom = "Youssef Had";
      chercheur1.post = "IT";
      chercheur1.ordNum = 1;

      //13
      chercheur1.afficher();

      //14
      Chercheur chercheur2;
      chercheur2 = new Chercheur("Ayoub Had", "Bouzbal", 100);

      //15
      // chercheur.afficher();
      System.out.println(chercheur2);

      //16
      System.out.println(chercheur1.nbr);

      //18 
      chercheur1.ordNum = 400;

      //19
      Chercheur chercheur3 = new Chercheur();

      //20
      chercheur3.nom = "Youssef Had";
      chercheur3.post = "IT";
      chercheur3.ordNum = 1;

      //21
      Chercheur[] chercheurs = {chercheur1,chercheur2,chercheur3};
      Bureau b1 = new Bureau("1","B1", chercheurs);
      Bureau b2 = new Bureau("2","B2", chercheurs);

      //22
      Bureau[] bureaux = {b1,b2};
      Laboratoire lab1 = new Laboratoire("laboratoire1", bureaux);

      //23
      System.out.println(lab1);
   }
}