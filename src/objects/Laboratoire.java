package objects;

/**
 * Laboratoire
 */
public class Laboratoire {

   public String adresse;
   public Bureau[] bureaux;

   public Laboratoire() {
   }

   public Laboratoire(String adresse, Bureau[] bureaux) {
      this.adresse = adresse;
      this.bureaux = bureaux;
   }

   public String toString() {
      String result = "Lab : "+adresse;
      //
      for (Bureau bureau : bureaux) {
         result += "\n\tBureau : "+bureau.toString();
      }
      //
      return result;
   }
}