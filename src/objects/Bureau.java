package objects;

/**
 * Bureau
 */
public class Bureau {

   public String code;
   public String nom;
   public Chercheur[] chercheurs = new Chercheur[5];

   public Bureau() { 
   }

   public Bureau(String code, String nom, Chercheur[] chercheurs) {
      this.code = code;
      this.nom = nom;
      this.chercheurs = chercheurs;
   }

   public String toString() {
      String result = "code : "+code+" nom : "+nom;
      //
      for (Chercheur chercheur : chercheurs) {
         result += "\n\t\tChercheur : "+chercheur.toString();
      }
      //
      return result;
   }
}