package objects;

/**
 * Chercheur
 */
public class Chercheur {

   // static int nbr = 0;
   public static int nbr=0;
   public String nom;
   public String post;
   public int ordNum;

   public Chercheur() {
      nbr++;
   }

   //14
   public Chercheur(String nom, String post, int ordNum) {
      this.nom = nom;
      this.post = post;
      this.ordNum = ordNum;
      //
      nbr++;
   }

   public String toString() {
      return "nom: " + nom + " post: " + post + " Ville: " + ordNum;
   }

   public void afficher() {
      System.out.println(this.toString());
   }

   //17
   public void comparer(Chercheur c) {
      if(this.equals(c))
         System.out.println("Les objets sont les même");
      else System.out.println("Les objets sont pas les même");
   }



}