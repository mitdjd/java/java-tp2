/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

/**
 *
 * @author HP
 */
public class Adresse {
    
    public int codePostal;
    public String gouvernorat;
    public String ville;
    
    public Adresse() {
    }
    
    public Adresse(int codePostal, String gouvernorat, String ville) {
        this.codePostal = codePostal;
        this.gouvernorat = gouvernorat;
        this.ville = ville;
    }
    
    public void afficher()
    {
    	System.out.println("Code postal: "+codePostal+" gouvernorat: "+gouvernorat+" Ville: "+ ville);
    }
    
    public void  modifier(int c) {
    	
		codePostal=c;
	}

    public String toString()
    {
    	return "Code postal: "+codePostal+" gouvernorat: "+gouvernorat+" Ville: "+ ville;
    }
    
}
